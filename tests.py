from random import randint
from fractions import Fraction

def check(order, minions):
    """Given a list of minions and their parameters, return the expect amount of interrogation time.

    It is necessary to use Fraction class to prevent rounding errors.
    order is a list of integers corresponding to the indices of the minions, i.e. what order to question them.
    minions is a list of lists as described in the problem statement.
    """
    cum, p, expected = 0, Fraction(0,1), Fraction(0,1)

    # step through minions in reverse order
    for o in order[:-1]:
        time, numer, denom = minions[o]     # get minion's parameters
        cum += time                         # track cumulative time taken
        myP = Fraction(numer,denom)*(1-p)   # let myP be probability that this minion is finally the one who talks
        expected += myP * cum               # let expected be the expected amount of time this minion adds
                                            #   i.e., probability of this one talking * total time taken
        p += myP                            # let p be the probability that any minion has talked so far

    # the final minion is assured of talking, so we assign remaining probability to it
    expected += (1 - p) * (cum + minions[order[-1]][0])
    return expected

def answer(minions):
    """This function is as described in problem statement.

    Essentially, all this problem requires is sorting the list of minions
    by expected interrogation time (probability_of_talking * time_to_interrogate).
    """
    r = []
    left = range(len(minions))

    # while there are any minions remaining to select from
    while left:
        bestX, bestN = 100000000000, None

        # identify the index of the minion with the best expected interrogation time
        #   i.e. lowest probability_of_talking * time_to_interrogate
        for n in left:
            time, numer, denom = minions[n]
            x = Fraction(time * denom, numer)
            if x < bestX:
                bestX, bestN = x, n

        # add the index of that minion to the list 'r', and remove it from the list 'left'
        r.append(bestN)
        left.remove(bestN)

    return r


# the tests provided in problem statement
tests = [
    # ([[5,1,5], [10,1,2]], [1,0]),
    # ([[5,1,5], [10,1,2], [15,1,10]], [1,0,2]),
    # ([[390,185,624],[686,351,947],[276,1023,1024],[199,148,250]], [2,3,0,1]),
    ([[1,10,58],[5,9,10],[500,1,1000]], [1,0,2]),
]

print (int(check(tests[0][1], tests[0][0])))
print (int(check([0,1,2], tests[0][0])))

