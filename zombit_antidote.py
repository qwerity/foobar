readme = """
Zombit antidote
===============

Forget flu season. Zombie rabbits have broken loose and are terrorizing Silicon Valley residents! Luckily, you've managed to steal a zombie antidote and set up a makeshift rabbit rescue station. Anyone who catches a zombie rabbit can schedule a meeting at your station to have it injected with the antidote, turning it back from a zombit to a fluffy bunny. Unfortunately, you have a limited amount of time each day, so you need to maximize these meetings. Every morning, you get a list of requested injection meetings, and you have to decide which to attend fully. If you go to an injection meeting, you will join it exactly at the start of the meeting, and only leave exactly at the end.

Can you optimize your meeting schedule? The world needs your help!

Write a method called answer(meetings) which, given a list of meeting requests, returns the maximum number of non-overlapping meetings that can be scheduled. If the start time of one meeting is the same as the end time of another, they are not considered overlapping.

meetings will be a list of lists. Each element of the meetings list will be a two element list denoting a meeting request. The first element of that list will be the start time and the second element will be the end time of that meeting request.

All the start and end times will be non-negative integers, no larger than 1000000.
The start time of a meeting will always be less than the end time.

The number of meetings will be at least 1 and will be no larger than 100.
The list of meetings will not necessarily be ordered in any particular fashion.

"""

def remove_overlaps(meeting, meetings):
    without_overlaps = []
    for m in meetings:
        overlapping = False

        if meeting[0] == m[1] or m[0] == meeting[1]:
            without_overlaps.append(m)
            continue
        if meeting[0] == m[0] or meeting[1] == m[1]:
            overlapping = True
        if m[0] <= meeting[0] < m[1]:
            overlapping = True
        if m[0] < meeting[1] <= m[1]:
            overlapping = True
        if meeting[0] <= m[0] < meeting[1]:
            overlapping = True
        if meeting[0] < m[1] <= meeting[1]:
            overlapping = True
        if not overlapping:
            without_overlaps.append(m)

    return without_overlaps


def shortest_meeting(meetings):
    shortest = meetings[0]

    for l in meetings:
        if l[1] < shortest[1]:
            shortest = l

    return shortest


def answer(meetings):
    if len(meetings) == 1:
        return 1

    remaining_meetings = meetings[:]
    remaining_meetings.sort()
    print (remaining_meetings)

    non_overlapping_meetings_count = 0

    while remaining_meetings:
        shortest = shortest_meeting(remaining_meetings)

        remaining_meetings = remove_overlaps(shortest, remaining_meetings)

        non_overlapping_meetings_count += 1

    return non_overlapping_meetings_count


if __name__ == '__main__':
    q = [[0, 1], [1, 2], [2, 3], [3, 5], [4, 5], [0, 1]]
    p = [[0, 1000000], [42, 43], [0, 1000000], [42, 43]]
    c = [[0, 1000000]]

    print (answer(q))
