readme = """
Minion interrogation
====================

You think you have Professor Boolean's password to the control center. All you need is confirmation, so that you can use it without being detected. You have managed to capture some minions so you can interrogate them to confirm.

You also have in your possession Professor Boolean's minion interrogation machine (yes, he interrogates his own minions). Its usage is simple: you ask the minion a question and put him in the machine. After some time (specific to the minion), you stop the machine and ask the minion for the answer. With certain probability (again, specific to the minion) you either get the truthful answer or the minion remains silent. Once you have subjected a minion to the machine, you cannot use it on the minion again for a long period.

The machine also has a 'guarantee' setting, which will guarantee that the minion will answer the question truthfully. Unfortunately, that has the potential to cause the minion some irreversible brain damage, so you vow to only use it as a last resort: on the last minion you interrogate.

Since Professor Boolean's password is periodically changed, you would like to know the answer as soon as possible. So you decide to interrogate the minions in an order which will take the least expected time (you can only use the machine on one minion at a time).

For example, you have captured two minions: minion A taking 10 minutes, and giving the answer with probability 1/2, and minion B taking 5 minutes, but giving the answer with probability 1/5.

If you interrogate A first, then you expect to take 12.5 minutes.
If you interrogate B first, then you expect to take 13 minutes and thus must interrogate A first for the shortest expected time for getting the answer.

Write a function answer(minions) which given a list of the characteristics of each minion, returns the lexicographically smallest ordering of minions, which gives you the smallest expected time of confirming the password.

The minions are numbered starting from 0.

The minions parameter will be a list of lists.

minions[i] will be a list containing exactly three elements, corresponding to the i^th minion.

The first element of minion[i] will be a positive integer representing the time the machine takes for that minion.

The ratio of the second and third elements will be the probability of that minion giving you the answer: the second element, a positive integer will be the numerator of the ratio and the third element, also a positive integer will be the denominator of the ratio. The denominator will always be greater than the numerator. That is, [time, numerator, denominator].

The return value must be a list of minion numbers (which are integers), depicting the optimal order in which to interrogate the minions. Since there could be multiple optimal orderings, return the lexicographically first optimal list.

There will be at-least two and no more than 50 minions.
All the integers in the input will be positive integers, no more than 1024.

Test cases
==========

Inputs:
(int) minions = [[5, 1, 5], [10, 1, 2]]
Output:
(int list) [1, 0]

Inputs:
(int) minions = [[390, 185, 624], [686, 351, 947], [276, 1023, 1024], [199, 148, 250]]
Output:
(int list) [2, 3, 0, 1]

// Solution
Here is one way to approach it. You're going to generate a "magic number" (somehow) for each minion such that sorting them by that number gives you the optimal solution. To hijack (and slightly change) TonySu's notation, the expected waiting time is:

T(1) + T(2)*P(1) + T(3)*P(1:2) + ... + T(n)*P(1:n-1)

where P(a:b) = P(a)*P(a+1)*...*P(b). Now, since this is the optimal ordering, it should be the case that it gets worse (or equal) if I swap any two minions. Somewhere in the sequence, we'll turn this:

... + T(a)*P(1:a-1) + T(b)*P(1:a-1)P(a) + ...

into this:

... + T(b)*P(1:a-1) + T(a)*P(1:a-1)P(b) + ...

I'm claiming that the top one is smaller than or equal to the bottom one, so if I subtract one from the other we get:

P(1:a-1) * (T(a)-T(b) + T(b)P(a) - T(a)P(b)) <= 0

After rearranging some terms we get:

T(a)/(1-P(a)) <= T(b)/(1-P(b))

So apparently, the magic number to sort by is the wait time divided by the probability that you'll get an answer
for each minion. 10 / 1/2 = 20 and 5 / 1/5 = 25, so in your case you should interrogate minion A first and B second.
 You already knew that, but this shows you how.

Edit: You might be thinking that while swapping two adjacent minions will clearly make my solution worse,
it might be possible to rearrange things in some other way that makes it better.
Let's pretend for the moment that this is indeed the case.
Then somewhere in the "real" optimal solution S there is a place where some minion X directly follows a minion
Y with a higher magic number. You can use the above reasoning to see that solution S can apparently be improved by
swapping X and Y, so apparently S wasn't optimal at all. Assuming that there was an optimal solution that is not
sorted by this magic number led to an obvious contradiction, so apparently the assumption was false.

TonySu defined P(i) as the probability that minion i does not speak. This is far more convenient for the majority
of the calculation. You gave us the probability that a minion will speak, which is 1 minus the probability that he won't.

"""

# from __future__ import division

def answer(n):
    i = 0
    minions = dict()
    for t, n, d in n:
        minions[i] = t * d / n
        i += 1

    print (minions)
    return [key for (key, value) in sorted(minions.items(), key=lambda x:x[1])]


minions = [[5, 1, 5], [10, 1, 2]]
minions = [[390, 185, 624], [686, 351, 947], [276, 1023, 1024], [199, 148, 250]]
minions = [[1,10,58],[5,9,10],[500,1,1000]]

print (answer(minions))

